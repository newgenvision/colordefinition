package com;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        File pricetagsFolder = new File("src/main/resources");
        File yellowPricetags = new File("resources/yellow");
        File redPricetags = new File("resources/red");
        final int redThreshold = 150;
        final int greenThreshold = 115;
        final int blueThreshold = 115;
        if (!yellowPricetags.exists())
        {
            yellowPricetags.mkdirs();
        }
        if (!redPricetags.exists())
        {
            redPricetags.mkdirs();
        }
        File[] pricetags = pricetagsFolder.listFiles();
        if (pricetags != null) {
            for (File pricetag : pricetags) {
                BufferedImage image = null;
                int[] color = new int[]{};
                try {
                    image = ImageIO.read(pricetag);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(image != null){
                    color = ColorThief.getColor(image);
                }
                if(color.length>0) {
                    if (color[0] > redThreshold && color[1] < greenThreshold && color[2] < blueThreshold) {
                        String path = redPricetags + "/" + pricetag.getName();
                        pricetag.renameTo(new File(path));
                    } else {
                        String path = yellowPricetags + "/" + pricetag.getName();
                        pricetag.renameTo(new File(path));
                    }
                }
            }
        } else {
            System.out.println("There are no files in pointed directory");
        }


    }
}
